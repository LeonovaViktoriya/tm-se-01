package ru.leonova.tm;

import java.util.LinkedList;
import java.util.Scanner;

public class TaskManager {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("***WELCOME TO TASK MANAGER***");
        String a = in.next();
        LinkedList<String> projectNames = new LinkedList<String>();
        LinkedList<String> taskNames = new LinkedList<String>();
        String s1 = "ENTER NAME:";
        if(a.equalsIgnoreCase("project-create")){
            String s = "[PROJECT CREATE]";
            create(s,s1,projectNames);
        }
        if(a.equalsIgnoreCase("project-list")){
            String s = "[PROJECT LIST]";
            list(s, projectNames);
        }
        if(a.equalsIgnoreCase("task-create")){
            String s = "[TASK CREATE]";
            create(s,s1,taskNames);
        }
        if(a.equalsIgnoreCase("task-list")){
            String s = "[TASK LIST]";
            list(s, taskNames);
        }
        if(a.equalsIgnoreCase("task-clear")){
            taskNames.clear();
        }
        if(a.equalsIgnoreCase("project-clear")){
            taskNames.clear();
        }
        if(a.equalsIgnoreCase("help")){
            System.out.println("help: Show all command");
        }

    }

    private static void create(String s, String s1, LinkedList<String> projectNames){
        Scanner in = new Scanner(System.in);
        System.out.println(s);
        System.out.println(s1);
        String name = in.next();
        projectNames.add(name);
    }

    private static void list(String s, LinkedList<String> projectNames){
        System.out.println(s);
        int i=0;
        for (String name: projectNames) {
            i++;
            System.out.println(i+"."+ name);
        }
    }
}
